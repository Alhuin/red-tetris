/** @type {import('@jest/types').Config.InitialOptions} */
const config = {
  verbose: true,
  clearMocks: true,
}
/* eslint-disable-next-line no-undef */
module.exports = config
