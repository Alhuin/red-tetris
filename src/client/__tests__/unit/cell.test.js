/**
 * @jest-environment jsdom
 */
import React from "react"
import { screen, render } from "@testing-library/react"

import Cell from "../../components/Tetris/Cell.js"

describe("CELL COMPONENT", () => {
  test("1: Empty cell, no shadow", () => {
    // WHEN
    render(<Cell key={0} type={0} shadow={false} posY={1} />)
    const cell = screen.getByTestId("cell")
    // THEN
    expect(cell).toBeInTheDocument()
    expect(cell).toHaveStyle(`
      width: auto;
      background: rgba(0, 0, 0, 0.8);
      border-style: solid;
      border-width: 4px;
      border-color: rgba(0,0,0,1) rgba(0,0,0,1) rgba(0,0,0,.1) rgba(0,0,0,.3);
    `)
  })

  test("2: Empty cell with shadow", () => {
    // WHEN
    render(<Cell key={0} type={0} shadow={true} posY={1} />)
    const cell = screen.getByTestId("cell")
    // THEN
    expect(cell).toBeInTheDocument()
    expect(cell).toHaveStyle(`
      width: auto;
      background: rgba(0, 0, 0, 0.8);
      border-style: solid;
      border-width: 1px 4px 4px 4px;
      border-color: rgba(250,0,0,1) rgba(0,0,0,1) rgba(0,0,0,.1) rgba(0,0,0,.3);
    `)
  })

  test("3: Colored cell, no shadow", () => {
    // WHEN
    render(<Cell key={0} type={"J"} shadow={false} posY={1} />)
    const cell = screen.getByTestId("cell")
    // THEN
    expect(cell).toBeInTheDocument()
    expect(cell).toHaveStyle(`
      width: auto;
      background: rgba(36, 95, 223, 0.8);
      border-style: solid;
      border-width: 4px;
      border-color: rgba(36,95,223,1) rgba(36,95,223,1) rgba(36,95,223,.1) rgba(36,95,223,.3);
    `)
  })

  test("4: Colored cell with shadow", () => {
    // WHEN
    render(<Cell key={0} type={"Z"} shadow={true} posY={1} />)
    const cell = screen.getByTestId("cell")
    // THEN
    expect(cell).toBeInTheDocument()
    expect(cell).toHaveStyle(`
      width: auto;
      background: rgba(227, 78, 78, 0.8);
      border-style: solid;
      border-width: 1px 4px 4px 4px;
      border-color: rgba(250,0,0,1) rgba(227,78,78,1) rgba(227,78,78,.1) rgba(227,78,78,.3);
    `)
  })
})
