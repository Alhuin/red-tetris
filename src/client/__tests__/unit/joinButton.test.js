/**
 * @jest-environment jsdom
 */
import React from "react"
import { screen, render } from "@testing-library/react"
import userEvent from "@testing-library/user-event"

import JoinButton from "../../components/Login/JoinButton.js"

describe("JOIN_BUTTON COMPONENT", () => {
  test("1: Renders without props", () => {
    render(<JoinButton />)
    const joinButton = screen.getByTestId("join button")

    expect(joinButton).toBeInTheDocument()
    expect(joinButton).toHaveTextContent("Join room")
  })

  test("2: Renders with props, trigger cb on click", async () => {
    // WITH
    const onClickMock = jest.fn()
    render(<JoinButton cb={onClickMock} />)
    const joinButton = screen.getByTestId("join button")
    // THEN
    expect(joinButton).toBeInTheDocument()
    expect(joinButton).toHaveTextContent("Join room")
    expect(onClickMock).not.toHaveBeenCalled()
    // WHEN
    const user = userEvent.setup()
    await user.click(joinButton, new MouseEvent("click"))
    expect(onClickMock).toHaveBeenCalledTimes(1)
  })

  test("3: Renders without props triggers default cb", async () => {
    // WITH
    jest.spyOn(console, "log").mockImplementation()
    render(<JoinButton />)
    const joinButton = screen.getByTestId("join button")
    // THEN
    expect(joinButton).toBeInTheDocument()
    expect(joinButton).toHaveTextContent("Join room")
    expect(console.log).not.toHaveBeenCalled()
    // WHEN
    const user = userEvent.setup()
    await user.click(joinButton, new MouseEvent("click"))
    expect(console.log).toHaveBeenCalledTimes(1)
  })
})
