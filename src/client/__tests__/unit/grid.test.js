/**
 * @jest-environment jsdom
 */
import React from "react"
import { screen, render } from "@testing-library/react"

import { initGrid } from "../../components/Tetris/helpers.js"
import Grid from "../../components/Tetris/Grid.js"

describe("GRID COMPONENT", () => {
  render(<Grid grid={initGrid()} />)
  const grid = screen.getByTestId("grid")
  const cells = screen.getAllByTestId("cell")

  test("1: Is mounted", () => {
    expect(grid).toBeInTheDocument()
  })

  test("2: Has good cells number", () => {
    expect(cells.length).toBe(200)
  })

  test("3: Has good style", () => {
    expect(grid).toHaveStyle(`
      width: 100%;
      display: grid;
      grid-template-rows: repeat( 20, calc(25vw / 10) );
      grid-template-columns: repeat(10,1fr);
      border: 2px solid #333;
      max-width: 25vw;
      background: #111;
    `)
  })

  test("4: Crash with bad params", () => {
    // Used to not pollute the test
    jest.spyOn(console, "error").mockImplementation()

    const bad_params_mounting = () => render(<Grid />)
    expect(bad_params_mounting).toThrowError()
  })
})
