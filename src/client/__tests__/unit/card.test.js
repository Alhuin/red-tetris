/**
 * @jest-environment jsdom
 */
import React from "react"
import { screen, render } from "@testing-library/react"

import Card from "../../components/Tetris/Card.js"

describe("CARD COMPONENT", () => {
  test("0: Default card, no props", () => {
    render(<Card />)
    const card = screen.getByTestId("card")
    expect(card).toBeInTheDocument()
    expect(card).toHaveTextContent("Info")
    expect(card).toHaveStyle(`color: #999;`)
  })

  test("1: Game over card winner", () => {
    render(<Card gameOver={true} looser={false} />)
    const card = screen.getByTestId("card")
    expect(card).toBeInTheDocument()
    expect(card).toHaveTextContent("You won !")
    expect(card).toHaveStyle(`color: green;`)
  })

  test("2: Game over card looser", () => {
    render(<Card gameOver={true} looser={true} />)
    const card = screen.getByTestId("card")
    expect(card).toBeInTheDocument()
    expect(card).toHaveTextContent("You loose !")
    expect(card).toHaveStyle(`color: red;`)
  })

  test("3: Text card", () => {
    render(<Card text={"Card used for score, lines and level"} />)
    const card = screen.getByTestId("card")
    expect(card).toBeInTheDocument()
    expect(card).toHaveTextContent("Card used for score, lines and level")
    expect(card).toHaveStyle(`color: #999;`)
  })
})
