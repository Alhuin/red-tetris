/**
 * @jest-environment jsdom
 */
import React from "react"
import { screen, render } from "@testing-library/react"
import PiecesInput from "../../components/GameSettings/PiecesInput"

describe("PIECES_INPUT COMPONENT", () => {
  test("1: Renders with props", () => {
    const setTetriminos = jest.fn()
    render(<PiecesInput tetriminos={"IJOS"} setTetriminos={setTetriminos} />)
    const piecesInput = screen.getByTestId("pieces input")

    expect(piecesInput).toBeInTheDocument()
  })
})
