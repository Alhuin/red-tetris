/**
 * @jest-environment jsdom
 */
import React from "react"
import { screen, render } from "@testing-library/react"
import userEvent from "@testing-library/user-event"

import StartButton from "../../components/Tetris/StartButtton.js"

describe("START_BUTTON COMPONENT", () => {
  test("1: Renders without props", () => {
    render(<StartButton />)
    const startButton = screen.getByTestId("start button")

    expect(startButton).toBeInTheDocument()
    expect(startButton).toHaveTextContent("Start")
  })

  test("2: Renders with props, trigger cb on click", async () => {
    // WITH
    const onClickMock = jest.fn()
    render(<StartButton cb={onClickMock} />)
    const startButton = screen.getByTestId("start button")
    expect(onClickMock).not.toHaveBeenCalled()
    // WHEN
    const user = userEvent.setup()
    await user.click(startButton, new MouseEvent("click"))
    // THEN
    expect(startButton).toBeInTheDocument()
    expect(startButton).toHaveTextContent("Start")
    expect(onClickMock).toHaveBeenCalledTimes(1)
  })
})
