/**
 * @jest-environment jsdom
 */
import "core-js"
import React from "react"
import { screen, render } from "@testing-library/react"
import userEvent from "@testing-library/user-event"
import { Provider } from "react-redux"

import store from "../../store"
import HomeButton from "../../components/Tetris/HomeButton"

jest.mock("react-router-dom", () => {
  const originalModule = jest.requireActual("react-router-dom")

  return {
    __esModule: true,
    ...originalModule,
    useNavigate: jest.fn(),
  }
})

import { useNavigate } from "react-router-dom"

function renderWithContext(element) {
  render(<Provider store={store}>{element}</Provider>)
  return store
}

describe("HOME_BUTTON COMPONENT", () => {
  test("1: Renders", () => {
    renderWithContext(<HomeButton />)
    const homeButton = screen.getByTestId("home button")

    expect(homeButton).toBeInTheDocument()
  })

  test("2: Click Navigates and Dispatches", async () => {
    const mockNavigate = jest.fn()
    useNavigate.mockReturnValue(mockNavigate)
    const store = renderWithContext(<HomeButton />)
    const homeButton = screen.getByTestId("home button")
    const user = userEvent.setup()
    await user.click(homeButton, new MouseEvent("click"))

    expect(mockNavigate).toHaveBeenCalledWith("/", { replace: true })
    expect(store.getState().game.gameStatus).toEqual(0)
    expect(store.getState().auth.ready).toBeFalsy()
  })
})
