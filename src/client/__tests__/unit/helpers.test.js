/**
 * @jest-environment jsdom
 */

import { checkCollision, initGrid } from "../../components/Tetris/helpers"
import { TETRIMINOS } from "../../components/constants/tetriminos"

describe("Tetris helpers", () => {
  test("1: checkCollision returns false if move possible", () => {
    expect(
      checkCollision(
        {
          tetrimino: TETRIMINOS.J.shape,
          pos: { x: 3, y: 0 },
        },
        initGrid(),
        { x: 0, y: 1 }
      )
    ).toBeFalsy()
  })
  test("2: checkCollision returns true if move impossible", () => {
    expect(
      checkCollision(
        {
          tetrimino: TETRIMINOS.J.shape,
          pos: { x: 3, y: 0 },
        },
        initGrid(),
        { x: 0, y: -1 }
      )
    ).toBeTruthy()
  })
})
