/**
 * @jest-environment jsdom
 */
import React from "react"
import { screen, render } from "@testing-library/react"
import CheckboxInput from "../../components/GameSettings/CheckboxInput"

describe("CHECKBOX_INPUT COMPONENT", () => {
  test("1: Renders with props", () => {
    const onInput = jest.fn()
    render(<CheckboxInput label={"test"} checked={true} onInput={onInput} />)
    const checkboxInput = screen.getByTestId("checkbox input")

    expect(checkboxInput).toBeInTheDocument()
    expect(checkboxInput).toHaveStyle(`
    display: flex;
    justify-content: space-between;
    align-items: center;
    font-family: Pixel;
    padding: 3px;
    margin: 10px;
    color: #999;`)
  })
})
