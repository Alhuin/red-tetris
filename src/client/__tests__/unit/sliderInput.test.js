/**
 * @jest-environment jsdom
 */
import React from "react"
import { screen, render } from "@testing-library/react"
import SliderInput from "../../components/GameSettings/SliderInput"

describe("SLIDER_INPUT COMPONENT", () => {
  test("1: Renders with props", () => {
    const setValue = jest.fn()
    render(
      <SliderInput
        label={"test"}
        min={0}
        max={10}
        value={0}
        step={2}
        setValue={setValue}
      />
    )
    const sliderInput = screen.getByTestId("slider input")

    expect(sliderInput).toBeInTheDocument()
    expect(sliderInput).toHaveTextContent("test: 0")
    expect(sliderInput).toHaveStyle(`
    display: flex;
    align-items: center;
    justify-content: space-around;
    font-family: Pixel;
      `)
  })
})
