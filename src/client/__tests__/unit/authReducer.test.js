/**
 * @jest-environment jsdom
 */
import { setUsers, initUser } from "../../store/reducers/authSlice"
import store from "../../store"

describe("START_BUTTON COMPONENT", () => {
  test("1: setUsers updates users in redux", () => {
    const users = [
      { name: "usera", id: 1 },
      { name: "userb", id: 2 },
    ]
    store.dispatch(setUsers(users))
    expect(store.getState().auth.users).toEqual(users)
  })
  test("2: initUsers updates username and roomName in redux", () => {
    const username = "usera"
    const roomName = "rooma"
    store.dispatch(initUser({ username, roomName }))
    expect(store.getState().auth.username).toEqual(username)
    expect(store.getState().auth.roomName).toEqual(roomName)
  })
})
