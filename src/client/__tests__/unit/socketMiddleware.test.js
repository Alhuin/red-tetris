/**
 * @jest-environment jsdom
 */
import "core-js"
import store from "../../store"
import {
  checkRoomSocket,
  endGameSocket,
  getTetriminoSocket,
  joinRoomSocket,
  sendShadowSocket,
  startGameSocket,
} from "../../store/actions"
import { setGameStatus } from "../../store/reducers/gameSlice"
import { initUser } from "../../store/reducers/authSlice"

describe("SOCKET MIDDLEWARE", () => {
  store.getState().auth.socket.emit = jest.fn()
  test("1: dispatching joinRoomSocket emits joinRoom to socket", () => {
    store.dispatch(joinRoomSocket("test"))
    expect(store.getState().auth.socket.emit).toHaveBeenCalledWith(
      "JOIN_ROOM",
      "test",
      expect.any(Function)
    )
  })
  test("2: dispatching checkRoomSocket emits checkRoom to socket", () => {
    store.dispatch(
      checkRoomSocket({
        callback: () => {},
      })
    )
    expect(store.getState().auth.socket.emit).toHaveBeenCalledWith(
      "CHECK_ROOM_USER",
      { callback: expect.any(Function) },
      expect.any(Function)
    )
  })
  test("3: dispatching sendShadowSocket emits sendShadow to socket if gameStatus = 1", () => {
    store.dispatch(setGameStatus(1))
    store.dispatch(sendShadowSocket("test"))
    expect(store.getState().auth.socket.emit).toHaveBeenCalledWith(
      "SEND_SHADOW",
      "",
      "test"
    )
  })
  test("4: dispatching sendShadowSocket does not emit sendShadow to socket if gameStatus != 1", () => {
    store.dispatch(setGameStatus(0))
    store.dispatch(sendShadowSocket("test"))
    expect(store.getState().auth.socket.emit).not.toHaveBeenCalled()
  })
  test("5: dispatching startGameSocket emits startGame to socket", () => {
    store.dispatch(
      startGameSocket({ gravity: 1, tetriminosList: "I", tetriminoPreview: false })
    )
    expect(store.getState().auth.socket.emit).toHaveBeenCalledWith("START_GAME", {
      gravity: 1,
      tetriminosList: "I",
      tetriminoPreview: false,
    })
  })
  test("6: dispatching endGameSocket emits endGame to socket", () => {
    store.dispatch(endGameSocket())
    expect(store.getState().auth.socket.emit).toHaveBeenCalledWith("END_GAME")
  })
  test("7: dispatching getTetriminoSocket emits getTetrimino to socket", async () => {
    await store.dispatch(
      initUser({
        username: "test",
        roomName: "testRoom",
      })
    )
    store.dispatch(getTetriminoSocket("testPayload"))
    expect(store.getState().auth.socket.emit).toHaveBeenCalledWith(
      "GET_TETRIMINO",
      "testRoom",
      "testPayload"
    )
  })
})
