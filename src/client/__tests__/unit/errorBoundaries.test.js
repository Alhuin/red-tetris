/**
 * @jest-environment jsdom
 */
import React from "react"
import { screen, render } from "@testing-library/react"
import ErrorBoundaries from "../../components/ErrorBoundaries"

describe("ERROR_BOUNDARY COMPONENT", () => {
  test("1: Renders children if no error", () => {
    render(
      <ErrorBoundaries>
        <div data-testid={"child"}>No Error</div>
      </ErrorBoundaries>
    )
    const loader = screen.queryByTestId("loader")
    expect(loader).not.toBeInTheDocument()
    const child = screen.queryByTestId("child")
    expect(child).toBeInTheDocument()
  })
  test("2: Renders error message if a child throws error", () => {
    jest.spyOn(console, "error").mockImplementation()
    const ThrowError = () => {
      throw new Error("Test")
    }
    render(
      <ErrorBoundaries>
        <ThrowError />
      </ErrorBoundaries>
    )
    const loader = screen.queryByTestId("loader")
    expect(loader).toBeInTheDocument()
    expect(console.error).toHaveBeenCalled()
  })
})
