/**
 * @jest-environment jsdom
 */
import React from "react"
import { screen, render } from "@testing-library/react"
import PieceOption from "../../components/GameSettings/PieceOption"
import I from "../../assets/images/Tetris_piece_I.png"
import userEvent from "@testing-library/user-event"

describe("PIECE_OPTION COMPONENT", () => {
  test("1: Renders with props", () => {
    const onInput = jest.fn()
    render(
      <PieceOption
        value={"I"}
        checked={true}
        src={I}
        onInput={onInput}
        disabled={false}
      />
    )
    const pieceOption = screen.getByTestId("piece option")

    expect(pieceOption).toBeInTheDocument()
    expect(pieceOption).toHaveStyle(`
    display: flex;
    justify-content: space-between;
    align-items: center;
    border: 1px solid #333;
    border-radius: 4px;
    border-color: #999;
    padding: 3px;
    margin: 10px;`)
  })

  test("2: triggers callback on click", async () => {
    const onInput = jest.fn()
    render(
      <PieceOption
        value={"I"}
        checked={true}
        src={I}
        onInput={onInput}
        disabled={false}
      />
    )

    const pieceInput = screen.getByTestId("piece input")
    const user = userEvent.setup()
    await user.click(pieceInput, new MouseEvent("click"))

    expect(onInput).toHaveBeenCalledWith("I", false)
  })
})
