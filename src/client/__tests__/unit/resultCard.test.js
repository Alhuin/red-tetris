/**
 * @jest-environment jsdom
 */
import React from "react"
import { screen, render } from "@testing-library/react"

import ResultCard from "../../components/Tetris/ResultCard.js"

describe("RESULTCARD COMPONENT", () => {
  const user = {
    name: "John Doe",
    score: 4242,
    level: 42,
    lines: 42,
  }
  render(<ResultCard user={user} />)
  const rCard = screen.getByTestId("result-card")

  test("1: Is rendered", () => {
    expect(rCard).toBeInTheDocument()
  })

  test("2: Has good text", () => {
    expect(rCard).toHaveTextContent("John Doe scored 4242 points")
    expect(rCard).toHaveTextContent("Level: 43")
    expect(rCard).toHaveTextContent("Lines: 42")
  })

  test("3: Has good style", () => {
    expect(rCard).toHaveStyle(`
      box-sizing: border-box;
      margin: 0 0 20px 0;
      padding: 20px;
      border: 4px solid #333;
      min-height: 30px;
      width: 100%;
      color: #999;
      border-radius: 20px;
      background: black;
      font-family: Pixel;
      font-size: 0.8rem;
    `)
  })
})
