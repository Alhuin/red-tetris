/**
 * @jest-environment jsdom
 */
import React from "react"
import { screen, render } from "@testing-library/react"

import GameSettings from "../../components/GameSettings"

describe("GAME_SETTINGS COMPONENT", () => {
  test("1: Renders with props", () => {
    const setGravity = jest.fn()
    const setTetriminosList = jest.fn()
    const setTetriminoPreview = jest.fn()
    render(
      <GameSettings
        defaultOpen={true}
        gravity={1}
        setGravity={setGravity}
        tetriminosList={"IJOS"}
        setTetriminosList={setTetriminosList}
        tetriminoPreview={true}
        setTetriminoPreview={setTetriminoPreview}
      />
    )
    const gameSettings = screen.getByTestId("game settings")

    expect(gameSettings).toBeInTheDocument()
    expect(gameSettings).toHaveStyle(`
    box-sizing: border-box;
    display: flex;
    flex-direction: column;
    margin: 0 0 20px 0;
    padding: 20px;
    border: 4px solid #333;
    min-height: 30px;
    width: 100%;
    border-radius: 20px;
    background: black;
    font-family: Pixel;
    font-size: 0.8rem;
    width: 100%;
    color: #999;
    `)
  })
})
