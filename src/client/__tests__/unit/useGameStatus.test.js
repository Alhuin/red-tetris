import React from "react"
import { renderHook } from "@testing-library/react"
import { Provider } from "react-redux"
import PropTypes from "prop-types"

import useGameStatus from "../../hooks/useGameStatus"
import store from "../../store"

const ReduxProvider = ({ children }) => <Provider store={store}>{children}</Provider>

ReduxProvider.propTypes = {
  children: PropTypes.node.isRequired,
}

describe("useGameStatus Hook", () => {
  test("1: Renders", () => {
    const wrapper = ({ children }) => (
      <ReduxProvider reduxStore={store}>{children}</ReduxProvider>
    )
    renderHook(() => useGameStatus(), { wrapper })
  })
})
