/**
 * @jest-environment jsdom
 */
import React from "react"
import { screen, render } from "@testing-library/react"
import Lobby from "../../components/Tetris/Lobby"

describe("LOBBY COMPONENT", () => {
  test("1: Renders Users", () => {
    render(
      <Lobby
        users={[
          {
            name: "test",
            isAdmin: false,
          },
          {
            name: "test2",
            isAdmin: true,
          },
        ]}
      />
    )
    const lobby_users = screen.getAllByTestId("lobby user")
    expect(lobby_users).toHaveLength(2)
    expect(lobby_users[0]).toHaveTextContent("test")
    expect(lobby_users[1]).toHaveTextContent("test2 (Admin)")
  })
  test("2: Property Validation fails without users", () => {
    jest.spyOn(console, "error").mockImplementation()
    expect(() => {
      render(<Lobby />)
    }).toThrowError()
  })
})
