import "core-js"
import React from "react"
import { act, renderHook } from "@testing-library/react"
import { Provider, useDispatch } from "react-redux"
import PropTypes from "prop-types"

import store from "../../store"
import usePlayer from "../../hooks/usePlayer"
import { initGrid } from "../../components/Tetris/helpers"

jest.mock("react-redux", () => {
  return {
    __esModule: true,
    ...jest.requireActual("react-redux"),
    useDispatch: jest.fn(),
  }
})

const ReduxProvider = ({ children }) => <Provider store={store}>{children}</Provider>

ReduxProvider.propTypes = {
  children: PropTypes.node.isRequired,
}

describe("usePlayer Hook", () => {
  let result, mockDispatch

  beforeEach(() => {
    jest.clearAllMocks()
    mockDispatch = jest.fn()
    useDispatch.mockReturnValue(mockDispatch)
    const wrapper = ({ children }) => (
      <ReduxProvider reduxStore={store}>{children}</ReduxProvider>
    )
    const hook = renderHook(() => usePlayer(), { wrapper })
    result = hook.result
  })

  test("1: Renders and return the correct data", () => {
    const { pos, tetrimino, collided } = result.current[0]
    expect(pos).toEqual({ x: 3, y: 3 })
    expect(tetrimino).toEqual([[0]])
    expect(collided).toBeFalsy()
    expect(result.current[1]).toBeInstanceOf(Function)
    expect(result.current[2]).toBeInstanceOf(Function)
    expect(result.current[3]).toBeInstanceOf(Function)
  })

  test("2: updatePlayerPos updates the position and collision", () => {
    const updatePlayerPos = result.current[1]
    expect(result.current[0].pos).toEqual({ x: 3, y: 3 })
    expect(result.current[0].collided).toBeFalsy()
    act(() => updatePlayerPos({ x: 1, y: 0, collided: true }))
    expect(result.current[0].pos).toEqual({ x: 4, y: 3 })
    expect(result.current[0].collided).toBeTruthy()
  })

  test("3: rotateIfPossible returns true if possible", () => {
    const updatePlayerPos = result.current[1]
    const rotateIfPossible = result.current[3]
    act(() => updatePlayerPos({ x: 3, y: 0, collided: false }))
    act(() => expect(rotateIfPossible(initGrid(), 1)).toBeTruthy())
  })

  test("4: resetPlayer calls dispatch with getTetrimino", () => {
    const resetPlayer = result.current[2]
    expect(result.current[0].pos).toEqual({ x: 3, y: 3 })
    expect(result.current[0].collided).toBeFalsy()
    act(() => resetPlayer())
    expect(mockDispatch).toHaveBeenCalledWith({
      payload: expect.anything(),
      type: "socket/getTetrimino",
    })
  })
})
