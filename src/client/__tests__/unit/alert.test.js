/**
 * @jest-environment jsdom
 */
import React from "react"
import { screen, render } from "@testing-library/react"
import userEvent from "@testing-library/user-event"

import CustomAlert from "../../components/Login/Alert.js"

describe("START_BUTTON COMPONENT", () => {
  test("1: Renders with props", () => {
    render(
      <CustomAlert severity={"warning"} message={"TestMessage"} close={jest.fn()} />
    )
    const alert = screen.queryByRole("alert")
    expect(alert).toBeInTheDocument()
    expect(alert).toHaveTextContent("TestMessage")
    expect(alert).toHaveTextContent("X")
  })

  test("2: Renders with props, trigger cb on click", async () => {
    // WITH
    const closeMock = jest.fn()
    render(
      <CustomAlert severity={"warning"} message={"TestMessage"} close={closeMock} />
    )
    const alertCloseButton = screen.getByTestId("close-alert")
    expect(closeMock).not.toHaveBeenCalled()
    // WHEN
    const user = userEvent.setup()
    await user.click(alertCloseButton, new MouseEvent("click"))
    // THEN
    expect(closeMock).toHaveBeenCalledTimes(1)
  })
})
