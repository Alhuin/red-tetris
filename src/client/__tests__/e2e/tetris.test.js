/**
 * @jest-environment jsdom
 */
import "core-js"
import React from "react"
import { act, waitFor, render, screen } from "@testing-library/react"
import { Provider } from "react-redux"

import Tetris from "../../components/Tetris"
import store from "../../store"

jest.mock("react-router-dom", () => {
  const originalModule = jest.requireActual("react-router-dom")

  return {
    __esModule: true,
    ...originalModule,
    useNavigate: jest.fn(),
    useParams: jest.fn(),
  }
})

function renderWithContext(element) {
  render(<Provider store={store}>{element}</Provider>)
  return store
}

describe("TETRIS E2E", () => {
  let servconfig

  beforeAll(() => {
    servconfig = require("../../../server/index")
  })

  afterAll(() => {
    servconfig.server.close()
    servconfig.io.close()
  })

  const renderTetris = async (roomName, username) => {
    const { useParams, useNavigate } = require("react-router-dom")
    useNavigate.mockReturnValue(jest.fn())
    useParams.mockReturnValue({ roomName, username })
    await act(() => renderWithContext(<Tetris />))
  }

  test(`0: Renders`, async () => {
    await renderTetris("testRoom", "testUser")
    await waitFor(() => expect(screen.getByTestId("tetris")).toBeInTheDocument())
    expect(screen.getByTestId("grid")).toBeInTheDocument()
  })
})
