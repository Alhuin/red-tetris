/**
 * @jest-environment jsdom
 */
import "core-js"
import React from "react"
import { act, render, screen, waitFor } from "@testing-library/react"

import Login from "../../components/Login"
import { setError } from "../../store/reducers/authSlice"
import store from "../../store"
import userEvent from "@testing-library/user-event"

jest.mock("react-router-dom", () => ({
  __esModule: true,
  ...jest.requireActual("react-router-dom"),
  useNavigate: jest.fn(),
}))

jest.mock("react-redux", () => {
  return {
    __esModule: true,
    ...jest.requireActual("react-redux"),
    useDispatch: jest.fn(),
  }
})

import { useDispatch, Provider } from "react-redux"

function renderWithContext(element) {
  render(<Provider store={store}>{element}</Provider>)
  return store
}

describe("Login Component", () => {
  let store, mockDispatch

  beforeEach(() => {
    jest.clearAllMocks()
    mockDispatch = jest.fn()
    useDispatch.mockReturnValue(mockDispatch)
    store = renderWithContext(<Login />)
  })

  afterEach(() => {
    const socket = store.getState().auth.socket
    socket.destroy()
  })

  test("1: Error is displayed if store.auth.error is not empty", async () => {
    expect(store.getState().auth.error).toEqual("")
    expect(screen.queryByRole("alert")).not.toBeInTheDocument()

    await act(() => store.dispatch(setError("test")))
    await waitFor(() => expect(screen.queryByRole("alert")).toBeInTheDocument())
    const alertElement = screen.queryByRole("alert")

    expect(store.getState().auth.error).toEqual("test")
    expect(alertElement).toBeInTheDocument()
    expect(alertElement).toHaveTextContent("testX")
  })

  test("2: Dispatches joinRoomSocket with user input onClick joinBoutton", async () => {
    const joinBoutton = screen.getByTestId("join button")
    const user = userEvent.setup()
    await user.type(screen.getByTestId("username input"), "testUser")
    await user.type(screen.getByTestId("room input"), "testRoom")
    await user.click(joinBoutton, new MouseEvent("click"))

    expect(mockDispatch).toHaveBeenCalledWith({
      payload: {
        callback: expect.anything(),
        roomName: "testRoom",
        username: "testUser",
      },
      type: "socket/checkRoomUser",
    })
  })
})
