import "core-js"
import {
  startGameSocket,
  checkRoomSocket,
  endGameSocket,
  getTetriminoSocket,
  joinRoomSocket,
  leaveRoomSocket,
  pieceMergedSocket,
  sendGameDataSocket,
  sendShadowSocket,
  sendWallSocket,
} from "../store/actions"
import { initUser } from "../store/reducers/authSlice"
import { setGameStatus } from "../store/reducers/gameSlice"
import {
  CHECK_ROOM_USER,
  END_GAME,
  GET_TETRIMINO,
  JOIN_ROOM,
  LEAVE_ROOM,
  PIECE_MERGED,
  SEND_GAME_DATA,
  SEND_SHADOW,
  SEND_WALL,
  START_GAME,
} from "../../server/constants/events"

// The socket middleware handles redux and socketIo dispatching
// If one of those actions are catched, they are handled here
// Otherwise they are passed as standard actions to redux (like setters, see redux/actions/index.js)
const socketMiddleware = (store) => (next) => (action) => {
  const { dispatch, getState } = store
  const state = getState()
  const { socket } = state.auth

  switch (action.type) {
    case joinRoomSocket.toString():
      socket.emit(JOIN_ROOM, action.payload, (response) => {
        action.payload.cb(response)
        if (response.authorized) {
          dispatch(
            initUser({
              username: action.payload.username,
              roomName: action.payload.roomName,
            })
          )
        }
      })
      break
    case checkRoomSocket.toString():
      socket.emit(CHECK_ROOM_USER, action.payload, action.payload.callback)
      break
    case pieceMergedSocket.toString():
      socket.emit(PIECE_MERGED, state.auth.roomName)
      break
    case sendGameDataSocket.toString():
      if (state.game.gameStatus === 1) {
        socket.emit(SEND_GAME_DATA, state.auth.roomName, action.payload)
      }
      break
    case sendShadowSocket.toString():
      if (state.game.gameStatus === 1) {
        socket.emit(SEND_SHADOW, state.auth.roomName, action.payload)
      }
      break
    case sendWallSocket.toString():
      if (state.game.gameStatus === 1) {
        socket.emit(SEND_WALL, state.auth.roomName, action.payload)
      }
      break
    case startGameSocket.toString():
      socket.emit(START_GAME, action.payload)
      break
    case endGameSocket.toString():
      socket.emit(END_GAME)
      break
    case leaveRoomSocket.toString():
      dispatch(setGameStatus(0))
      socket.emit(LEAVE_ROOM, action.payload)
      break
    case getTetriminoSocket.toString(): {
      socket.emit(GET_TETRIMINO, state.auth.roomName, action.payload)
      break
    }
    default:
      console.log("the next action:", action)
      return next(action)
  }
}

export default socketMiddleware
