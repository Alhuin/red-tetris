import { useEffect, useState } from "react"
import { useSelector, useDispatch } from "react-redux"

import { checkCollision, GRID_WIDTH, initGrid } from "../components/Tetris/helpers"
import { selectGameStatus, selectSocket } from "../store/selectors"
import { SET_SHADOW, SET_WALL } from "../../server/constants/events"
import { setGameStatus } from "../store/reducers/gameSlice"
import { sendShadowSocket, endGameSocket, pieceMergedSocket } from "../store/actions"
import { TETRIMINOS } from "../components/constants/tetriminos"
let merged = false

const useGrid = (
  player,
  resetPlayer,
  nbPlayers,
  setDropTime,
  setScore,
  setLines,
  setLevel,
  setEndGameData,
  gridRef,
  setPlayer,
  setGravity,
  setTetriminoPreview
) => {
  const dispatch = useDispatch()
  const [linesCleared, setLinesCleared] = useState(0)
  const [grid, setGrid] = useState(initGrid())
  const socket = useSelector(selectSocket)
  const gameStatus = useSelector(selectGameStatus)
  const [opponentShadow, setOpponentShadow] = useState([])
  const [wall, setWall] = useState(0)

  const resetScores = () => {
    setScore(0)
    setLines(0)
    setLevel(0)
  }

  const startGame = ({ gravity, tetriminoPreview }) => {
    // reset grid, player, gameStatus & dropTime
    resetScores()
    setOpponentShadow([])
    setTetriminoPreview(tetriminoPreview)
    setGravity(gravity)
    setDropTime(gravity ? (0.8 ** 0 * 1000) / gravity : 0)
    resetPlayer()
    merged = false
    dispatch(setGameStatus(1))
    if (gridRef && gridRef.current) {
      setTimeout(() => gridRef.current.focus(), 50)
    }
  }

  const endGame = () => {
    setDropTime(null)
    setOpponentShadow([])
    setPlayer({
      pos: { x: GRID_WIDTH / 2 - 2, y: 0 },
      tetrimino: TETRIMINOS[0].shape,
      collided: false,
    })
    dispatch(setGameStatus(2))
    setGrid(initGrid())
  }

  const managePlayerOverlap = (grid, player) => {
    let overlap = 0
    while (
      checkCollision(player, grid, { x: 0, y: -overlap }) &&
      player.pos.y - overlap >= 0
    ) {
      overlap++
    }
    return overlap
  }

  const buildShadow = (clearedGrid) => {
    const shadow = []
    for (let i = 0; i < GRID_WIDTH; i += 1) {
      shadow.push(
        clearedGrid.map((line) => line[i]).findIndex((cell) => cell[0] !== 0)
      )
    }
    return shadow
  }

  const isLooser = (grid) => grid[3].findIndex((cell) => cell[1] === "merged") !== -1

  // builds a fresh grid from the stored one (with merged tetriminos) and draws the current piece
  useEffect(() => {
    setLinesCleared(0)

    const removeClearedLines = (newGrid) =>
      newGrid.reduce((ack, line) => {
        if (line.findIndex((cell) => cell[0] === 0 || cell[0] === "W") === -1) {
          // if we find a line with no 0 (full)
          // increment linesCleared number
          setLinesCleared((prev) => prev + 1)
          // add a new empty line at the top of the grid
          ack.unshift(new Array(newGrid[0].length).fill([0, "clear", 0]))
          return ack
        }
        ack.push(line)
        return ack
      }, [])

    const updateGrid = (prevGrid) => {
      if (gameStatus !== 1) {
        return initGrid()
      }
      if (merged && !player.collided) {
        merged = false
      }
      // clear grid & keep merged pieces only
      let newGrid = prevGrid.map((line) =>
        line.map((cell) =>
          cell[1] === "clear"
            ? [0, "clear", 0] // if cell is not merged don't keep it in new grid
            : [cell[0], cell[1], 0]
        )
      )
      let overlap = 0
      // Manage shadow and walls
      if (nbPlayers > 1) {
        if (wall) {
          newGrid.splice(0, wall)
          for (let i = 0; i < wall; i += 1) {
            newGrid.push(new Array(newGrid[0].length).fill(["W", "merged", 0]))
          }
          overlap = managePlayerOverlap(newGrid, player)
          dispatch(sendShadowSocket(buildShadow(newGrid)))
          setWall(0)
        }
        opponentShadow.forEach((first, index) => {
          if (first >= 0) {
            newGrid[first][index][2] = 1
          }
        })
      }
      // Draw tetrimino on the fresh grid
      if (!merged) {
        player.tetrimino.forEach((line, y) => {
          line.forEach((value, x) => {
            if (value !== 0) {
              // not an empty cell
              if (
                newGrid[y + player.pos.y - overlap][x + player.pos.x][1] === "clear"
              ) {
                newGrid[y + player.pos.y - overlap][x + player.pos.x] = [
                  value,
                  `${player.collided || overlap ? "merged" : "clear"}`, // merge tetrimino if we collided
                  0,
                ]
              }
            }
          })
        })
      }
      const gameFinished = isLooser(newGrid)
      // Manage when collided
      if (player.collided || overlap) {
        if (overlap > 0 && !merged) {
          dispatch(pieceMergedSocket())
        }
        merged = true
        newGrid = removeClearedLines(newGrid)
        if (nbPlayers > 1) {
          dispatch(sendShadowSocket(buildShadow(newGrid)))
        }
        if (!gameFinished) {
          resetPlayer()
        }
      }
      if (gameFinished) {
        dispatch(endGameSocket())
      }
      return newGrid
    }

    // set new grid
    setGrid(updateGrid(grid))
  }, [player.pos, opponentShadow, wall])

  useEffect(() => {
    if (socket.listeners("start").length === 0) {
      socket.on("start", (settings) => startGame(settings))
      socket.on(SET_SHADOW, setOpponentShadow)
      socket.on(SET_WALL, setWall)
      socket.on("end", (data) => {
        setEndGameData(data)
        endGame()
      })
    }
  }, [])

  return [grid, linesCleared, startGame]
}

export default useGrid
