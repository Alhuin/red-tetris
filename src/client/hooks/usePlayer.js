import { useCallback, useState } from "react"

import { TETRIMINOS } from "../components/constants/tetriminos"
import { checkCollision, GRID_WIDTH } from "../components/Tetris/helpers"
import { getTetriminoSocket, pieceMergedSocket } from "../store/actions"
import { useDispatch } from "react-redux"

const usePlayer = (setNextTetrimino) => {
  const dispatch = useDispatch()
  const [player, setPlayer] = useState({
    pos: { x: GRID_WIDTH / 2 - 2, y: 4 - TETRIMINOS[0].height },
    tetrimino: TETRIMINOS[0].shape,
    collided: false,
  })

  const rotate = (matrix, direction) => {
    const rotatedTetrimino = matrix.map((_, index) =>
      matrix.map((col) => col[index])
    )
    return direction > 0
      ? rotatedTetrimino.map((line) => line.reverse())
      : rotatedTetrimino.reverse()
  }

  const rotateIfPossible = (grid, direction) => {
    const playerCopy = JSON.parse(JSON.stringify(player))
    const tmpX = playerCopy.pos.x
    let offset = 1

    playerCopy.tetrimino = rotate(playerCopy.tetrimino, direction)

    while (checkCollision(playerCopy, grid, { x: 0, y: 0 })) {
      playerCopy.pos.x += offset
      offset = -(offset + (offset > 0 ? 1 : 0))
      if (offset > playerCopy.tetrimino[0].length) {
        // rotation impossible
        rotate(playerCopy.tetrimino, -direction) // rotate -dir pour retrouver la pos initiale
        playerCopy.x = tmpX
        return false
      }
    }
    setPlayer(playerCopy)
    return true
  }

  const updatePlayerPos = ({ x, y, collided }) => {
    if (!player.collided && collided) {
      dispatch(pieceMergedSocket())
    }
    setPlayer({
      ...player,
      pos: {
        x: (player.pos.x += x),
        y: (player.pos.y += y),
      },
      collided,
    })
  }

  const resetPlayer = useCallback(() => {
    dispatch(
      getTetriminoSocket(({ tetrimino, nextTetrimino }) => {
        if (nextTetrimino) {
          setNextTetrimino(nextTetrimino)
        }
        tetrimino = tetrimino || 0
        setPlayer({
          pos: {
            x: GRID_WIDTH / 2 - 2, // horizontally center
            y: 4 - TETRIMINOS[tetrimino].height,
          },
          tetrimino: TETRIMINOS[tetrimino].shape,
          collided: false,
        })
      })
    )
  }, [])

  return [player, updatePlayerPos, resetPlayer, rotateIfPossible, setPlayer]
}

export default usePlayer
