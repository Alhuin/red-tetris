import React, { useCallback, useState } from "react"
import StyledTetriminoPreview from "../components/styles/StyledTetriminoPreview"
import I from "../assets/images/Tetris_piece_I.png"
import J from "../assets/images/Tetris_piece_J.png"
import O from "../assets/images/Tetris_piece_O.png"
import S from "../assets/images/Tetris_piece_S.png"
import T from "../assets/images/Tetris_piece_T.png"
import L from "../assets/images/Tetris_piece_L.png"
import Z from "../assets/images/Tetris_piece_Z.png"

const useTetriminoPreview = () => {
  const [nextTetrimino, setNextTetrimino] = useState(0)
  const [tetriminoPreview, setTetriminoPreview] = useState(false)

  const previews = {
    I: I,
    J: J,
    O: O,
    S: S,
    T: T,
    L: L,
    Z: Z,
  }

  const TetriminoPreview = useCallback(() => {
    return (
      tetriminoPreview &&
      nextTetrimino && (
        <StyledTetriminoPreview data-testid={"tetrimino preview"}>
          Next:
          <img src={previews[nextTetrimino]} alt={nextTetrimino} />
        </StyledTetriminoPreview>
      )
    )
  }, [tetriminoPreview, nextTetrimino])

  return [tetriminoPreview, setTetriminoPreview, setNextTetrimino, TetriminoPreview]
}

export default useTetriminoPreview
