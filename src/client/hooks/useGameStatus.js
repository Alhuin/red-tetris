import { useEffect, useCallback } from "react"
import { useDispatch } from "react-redux"

import { sendGameDataSocket, sendWallSocket } from "../store/actions"

const useGameStatus = (
  linesCleared,
  score,
  setScore,
  lines,
  setLines,
  level,
  nb_players
) => {
  const dispatch = useDispatch()
  const points = [40, 100, 300, 1200]
  const calcScore = useCallback(() => {
    if (linesCleared > 0) {
      const newScore = score + points[linesCleared - 1] * (level + 1)
      const newLines = lines + linesCleared
      setScore(newScore)
      setLines(newLines)
      dispatch(sendGameDataSocket({ score: newScore, lines: newLines, level }))
    }
  }, [lines, score, level, linesCleared])

  useEffect(() => {
    calcScore()
    if (linesCleared > 1 && nb_players > 1)
      dispatch(sendWallSocket(linesCleared - 1))
  }, [linesCleared])

  return []
}

export default useGameStatus
