import React, { useState } from "react"
import { useDispatch, useSelector } from "react-redux"

// Redux
import { selectError } from "../../store/selectors"
import { setError } from "../../store/reducers/authSlice"

// Components
import CustomAlert from "./Alert"
import JoinButton from "./JoinButton"
import { StyledLogin, StyledLoginWrapper } from "../styles/StyledLogin"
import StyledLogo from "../styles/StyledLogo"
import { useNavigate } from "react-router-dom"
import { checkRoomSocket } from "../../store/actions"

function Login() {
  const error = useSelector(selectError)

  const dispatch = useDispatch()
  const navigate = useNavigate()
  const [roomName, setRoomName] = useState("")
  const [username, setUsername] = useState("")

  const closeAlert = () => dispatch(setError(""))

  return (
    <StyledLoginWrapper data-testid={"login"}>
      <StyledLogo />
      <StyledLogin>
        <input
          type="text"
          placeholder="Login"
          onChange={(e) => setUsername(e.target.value)}
          data-testid={"username input"}
        />
        <input
          type="text"
          placeholder="Room name"
          onChange={(e) => setRoomName(e.target.value)}
          data-testid={"room input"}
        />
      </StyledLogin>
      <JoinButton
        cb={() => {
          dispatch(
            checkRoomSocket({
              username: username,
              roomName: roomName,
              callback: (response) => {
                if (response.authorized) {
                  closeAlert()
                  navigate(`${roomName}[${username}]`)
                } else {
                  dispatch(setError(response.error))
                }
              },
            })
          )
        }}
      />
      {error !== "" && (
        <CustomAlert severity="warning" message={error} close={() => closeAlert()} />
      )}
    </StyledLoginWrapper>
  )
}

export default Login
