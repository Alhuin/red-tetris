const I = "I"
const J = "J"
const O = "O"
const S = "S"
const T = "T"
const L = "L"
const Z = "Z"
const W = "W"

export const TETRIMINOS = {
  0: {
    // empty tetrimino, the first displayed on the grid at Tetris landing
    shape: [[0]],
    height: 1,
    color: "0,0,0",
  },
  I: {
    shape: [
      [0, I, 0, 0],
      [0, I, 0, 0],
      [0, I, 0, 0],
      [0, I, 0, 0],
    ],
    height: 4,
    color: "80, 227, 230",
  },
  J: {
    shape: [
      [0, J, 0],
      [0, J, 0],
      [J, J, 0],
    ],
    height: 3,
    color: "36, 95, 223",
  },
  O: {
    shape: [
      [O, O],
      [O, O],
    ],
    height: 2,
    color: "223, 217, 36",
  },
  S: {
    shape: [
      [0, S, S],
      [S, S, 0],
      [0, 0, 0],
    ],
    height: 2,
    color: "48, 211, 56",
  },
  T: {
    shape: [
      [T, T, T],
      [0, T, 0],
      [0, 0, 0],
    ],
    height: 2,
    color: "132, 61, 198",
  },
  L: {
    shape: [
      [0, L, 0],
      [0, L, 0],
      [0, L, L],
    ],
    height: 3,
    color: "223, 173, 36",
  },
  Z: {
    shape: [
      [Z, Z, 0],
      [0, Z, Z],
      [0, 0, 0],
    ],
    height: 2,
    color: "227, 78, 78",
  },
  W: {
    // wall tetrimino
    shape: [[W]],
    height: 1,
    color: "125,125,125",
  },
}
