import styled from "styled-components"
import tetris from "../../assets/images/tetris.png"

const StyledHomeButton = styled.button`
  position: absolute;
  height: 10vh;
  width: 15vh;
  margin-top: 3vh;
  margin-left: 3vh;
  background: url(${tetris});
  background-size: cover;
  cursor: pointer;
  border: none;
`

export default StyledHomeButton
