import styled from "styled-components"

const StyledCheckboxInput = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  font-family: Pixel;
  padding: 3px;
  margin: 10px;
  color: #999;
`

export default StyledCheckboxInput
