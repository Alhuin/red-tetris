import styled from "styled-components"

const StyledSliderInput = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-around;
  font-family: Pixel;
`

export default StyledSliderInput
