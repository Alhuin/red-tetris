import styled from "styled-components"

const StyledGameSettings = styled.div`
  box-sizing: border-box;
  display: flex;
  flex-direction: column;
  margin: 0 0 20px 0;
  padding: 20px;
  border: 4px solid #333;
  min-height: 30px;
  width: 100%;
  border-radius: 20px;
  background: black;
  font-family: Pixel;
  font-size: 0.8rem;
  width: 100%;
  color: #999;
`

export default StyledGameSettings
