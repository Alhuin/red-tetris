import styled from "styled-components"
import background from "../../assets/images/star_background.jpeg"

export const StyledApp = styled.div`
  background: url(${background});
  background-size: cover;
`
