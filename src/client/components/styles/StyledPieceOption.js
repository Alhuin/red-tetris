import styled from "styled-components"

const StyledPieceOption = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  border: 1px solid #333;
  border-radius: 4px;
  border-color: #999;
  padding: 3px;
  margin: 10px;
`

export default StyledPieceOption
