import styled from "styled-components"

const StyledTetriminoPreview = styled.div`
  box-sizing: border-box;
  display: flex;
  align-items: center;
  justify-content: space-around;
  margin: 0 0 20px 0;
  padding: 20px;
  border: 4px solid #333;
  width: 100%;
  border-radius: 20px;
  background: black;
  font-family: Pixel;
  font-size: 0.8rem;
  color: #999;
`

export default StyledTetriminoPreview
