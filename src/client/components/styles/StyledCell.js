import styled from "styled-components"

const StyledCell = styled.div`
  width: auto;
  background: rgba(${(props) => props.color}, 0.8);
  border-style: solid;
  border-width: ${(props) => (props.shadow ? "1px 4px 4px 4px;" : "4px;")}}
  border-color: ${(props) =>
    "rgba(" +
    (props.shadow ? "250, 0, 0" : props.color) +
    ", 1) " +
    "rgba(" +
    props.color +
    ", 1) " +
    "rgba(" +
    props.color +
    ", .1) " +
    "rgba(" +
    props.color +
    ", .3);"}
`

export default StyledCell
