import React from "react"
import PropTypes from "prop-types"
import Loader from "../Loader"

export class ErrorBoundaries extends React.Component {
  constructor(props) {
    super(props)
    this.state = { hasError: false }
  }

  static getDerivedStateFromError() {
    return { hasError: true }
  }

  componentDidCatch(error, errorInfo) {
    console.error(error, errorInfo)
  }

  render() {
    if (this.state.hasError) {
      return <Loader />
    }

    return this.props.children
  }
}
ErrorBoundaries.propTypes = {
  children: PropTypes.node.isRequired,
}
