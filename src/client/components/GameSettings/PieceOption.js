import React from "react"
import StyledPieceOption from "../styles/StyledPieceOption"
import PropTypes from "prop-types"

const PieceOption = ({ value, checked, src, onInput, disabled }) => {
  return (
    <StyledPieceOption data-testid={"piece option"}>
      <img alt={value} src={src} />
      <input
        data-testid={"piece input"}
        type="checkbox"
        id="checkbox"
        defaultChecked={checked}
        disabled={disabled}
        onInput={(e) => onInput(value, e.target.checked)}
      />
    </StyledPieceOption>
  )
}

PieceOption.propTypes = {
  value: PropTypes.string.isRequired,
  checked: PropTypes.bool.isRequired,
  disabled: PropTypes.bool.isRequired,
  src: PropTypes.string.isRequired,
  onInput: PropTypes.func.isRequired,
}

export default PieceOption
