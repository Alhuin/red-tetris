import React, { useState } from "react"
import StyledGameSettings from "../styles/StyledGameSettings"
import SliderInput from "./SliderInput"
import PiecesInput from "./PiecesInput"
import UpDownArrow from "../../assets/images/up-and-down-arrow.png"
import CheckboxInput from "./CheckboxInput"
import PropTypes from "prop-types"

const GameSettings = ({
  defaultOpen,
  gravity,
  setGravity,
  tetriminosList,
  setTetriminosList,
  tetriminoPreview,
  setTetriminoPreview,
}) => {
  const [open, setOpen] = useState(defaultOpen)

  return (
    <StyledGameSettings data-testid={"game settings"}>
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
        }}
      >
        Settings
        <img
          alt="collide_settings"
          src={UpDownArrow}
          onClick={() => setOpen(!open)}
          style={{ height: "20px", width: "20px", cursor: "pointer" }}
        />
      </div>
      {open && (
        <>
          <CheckboxInput
            label={"tetrimino preview"}
            checked={tetriminoPreview}
            onInput={setTetriminoPreview}
          />
          <SliderInput
            label={"gravity"}
            min={0}
            max={3}
            step={1}
            value={gravity}
            setValue={setGravity}
          />
          <PiecesInput
            tetriminos={tetriminosList}
            setTetriminos={setTetriminosList}
          />
        </>
      )}
    </StyledGameSettings>
  )
}

GameSettings.propTypes = {
  gravity: PropTypes.number.isRequired,
  setGravity: PropTypes.func.isRequired,
  tetriminosList: PropTypes.string.isRequired,
  setTetriminosList: PropTypes.func.isRequired,
  tetriminoPreview: PropTypes.bool.isRequired,
  setTetriminoPreview: PropTypes.func.isRequired,
  defaultOpen: PropTypes.bool.isRequired,
}
export default GameSettings
