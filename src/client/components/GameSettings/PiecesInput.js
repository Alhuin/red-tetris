import React from "react"
import PieceOption from "./PieceOption"
import I from "../../assets/images/Tetris_piece_I.png"
import J from "../../assets/images/Tetris_piece_J.png"
import O from "../../assets/images/Tetris_piece_O.png"
import S from "../../assets/images/Tetris_piece_S.png"
import T from "../../assets/images/Tetris_piece_T.png"
import L from "../../assets/images/Tetris_piece_L.png"
import Z from "../../assets/images/Tetris_piece_Z.png"
import PropTypes from "prop-types"

const PiecesInput = ({ tetriminos, setTetriminos }) => {
  const updateTetriminos = (value, selected) => {
    setTetriminos((prev) => (selected ? prev + value : prev.replace(value, "")))
  }
  const options = [
    { value: "I", src: I },
    { value: "J", src: J },
    { value: "O", src: O },
    { value: "S", src: S },
    { value: "T", src: T },
    { value: "L", src: L },
    { value: "Z", src: Z },
  ]

  return (
    <div data-testid={"pieces input"}>
      {options.map(({ value, src }) => {
        const checked = tetriminos.indexOf(value) !== -1

        return (
          <PieceOption
            key={value}
            value={value}
            checked={checked}
            src={src}
            onInput={updateTetriminos}
            disabled={tetriminos.length === 1 && checked}
          />
        )
      })}
    </div>
  )
}

PiecesInput.propTypes = {
  tetriminos: PropTypes.string.isRequired,
  setTetriminos: PropTypes.func.isRequired,
}

export default PiecesInput
