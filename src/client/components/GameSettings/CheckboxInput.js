import React from "react"
import StyledCheckboxInput from "../styles/StyledCheckboxInput"
import PropTypes from "prop-types"

const CheckboxInput = ({ label, checked, onInput }) => {
  return (
    <StyledCheckboxInput data-testid={"checkbox input"}>
      {label}
      <input
        type="checkbox"
        id="checkbox"
        defaultChecked={checked}
        onInput={(e) => onInput(e.target.checked)}
      />
    </StyledCheckboxInput>
  )
}

CheckboxInput.propTypes = {
  label: PropTypes.string.isRequired,
  checked: PropTypes.bool.isRequired,
  onInput: PropTypes.func.isRequired,
}

export default CheckboxInput
