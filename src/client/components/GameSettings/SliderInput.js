import React from "react"
import StyledSliderInput from "../styles/StyledSliderInput"
import PropTypes from "prop-types"

const SliderInput = ({ label, min, max, step, value, setValue }) => {
  return (
    <StyledSliderInput data-testid={"slider input"}>
      <p>
        {label}: {value}
      </p>
      <input
        name={label}
        type="range"
        min={min}
        max={max}
        step={step}
        value={value}
        onChange={(e) => setValue(parseInt(e.target.value))}
        color={"white"}
        style={{ width: "50%" }}
      />
    </StyledSliderInput>
  )
}

SliderInput.propTypes = {
  label: PropTypes.string.isRequired,
  min: PropTypes.number.isRequired,
  max: PropTypes.number.isRequired,
  step: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
  setValue: PropTypes.func.isRequired,
}

export default SliderInput
