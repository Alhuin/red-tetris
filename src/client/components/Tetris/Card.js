import React from "react"
import PropTypes from "prop-types"
import StyledCard from "../styles/StyledCard"

const Card = ({ gameOver, text, looser }) => {
  if (gameOver) {
    text = looser ? "You loose !" : "You won !"
  }

  return (
    <StyledCard gameOver={gameOver} looser={looser} data-testid="card">
      {text}
    </StyledCard>
  )
}

Card.propTypes = {
  gameOver: PropTypes.bool,
  looser: PropTypes.bool,
  text: PropTypes.string,
}

Card.defaultProps = {
  gameOver: false,
  looser: false,
  text: "Info",
}

export default Card
