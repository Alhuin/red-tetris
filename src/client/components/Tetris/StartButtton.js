import React from "react"
import PropTypes from "prop-types"
import StyledStartButton from "../styles/StyledStartButton"

function StartButton({ cb }) {
  return (
    <StyledStartButton type={"button"} onClick={cb} data-testid={"start button"}>
      Start
    </StyledStartButton>
  )
}

StartButton.propTypes = {
  cb: PropTypes.func,
}

StartButton.defaultProps = {
  cb: () => console.log("StartButton cb()"),
}

export default StartButton
