import React from "react"
import PropTypes from "prop-types"

import Cell from "./Cell"
import StyledGrid from "../styles/StyledGrid"

function Grid({ grid }) {
  return (
    <StyledGrid width={grid[0].length} height={grid.length - 4} data-testid={"grid"}>
      {grid.map(
        (line, y) =>
          y >= 4 &&
          line.map((cell, x) => (
            <Cell
              /* eslint-disable-next-line react/no-array-index-key */
              key={x}
              posY={y}
              type={cell[0]}
              shadow={cell[2] === 1}
            />
          ))
      )}
    </StyledGrid>
  )
}

Grid.propTypes = {
  grid: PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.array)).isRequired,
}

export default Grid
