import React, { memo } from "react"
import PropTypes from "prop-types"
import StyledCell from "../styles/StyledCell"
import { TETRIMINOS } from "../constants/tetriminos"

const Cell = ({ posY, type, shadow }) => (
  <StyledCell
    y={posY}
    type={type}
    color={TETRIMINOS[type].color}
    shadow={shadow}
    data-testid={"cell"}
  />
)

Cell.propTypes = {
  posY: PropTypes.number.isRequired,
  type: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  shadow: PropTypes.bool.isRequired,
}

export default memo(Cell)
