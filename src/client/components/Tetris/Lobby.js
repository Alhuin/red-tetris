import React from "react"
import PropTypes from "prop-types"
import StyledLobby from "../styles/StyledLobby"

function Lobby({ users }) {
  return (
    <StyledLobby data-testid={"lobby"}>
      Players:
      <ul>
        {users.map((user) => (
          <li key={user.name} data-testid={"lobby user"}>
            {user.name}
            {user.isAdmin ? " (Admin)" : ""}
          </li>
        ))}
      </ul>
    </StyledLobby>
  )
}

Lobby.propTypes = {
  users: PropTypes.arrayOf(PropTypes.object).isRequired,
}

export default Lobby
