import React from "react"
import StyledHomeButton from "../styles/StyledHomeButton"
import { useNavigate } from "react-router-dom"
import { leaveRoomSocket } from "../../store/actions"
import { useDispatch } from "react-redux"

const HomeButton = () => {
  const navigate = useNavigate()
  const dispatch = useDispatch()

  return (
    <StyledHomeButton
      onClick={() => {
        dispatch(leaveRoomSocket())
        navigate("/", { replace: true })
      }}
      data-testid={"home button"}
    />
  )
}

export default HomeButton
