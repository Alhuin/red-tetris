/* eslint-disable react/forbid-prop-types */
import React, { useEffect, useRef, useState } from "react"
import { useSelector, useDispatch } from "react-redux"

// Redux
import { useParams, useNavigate } from "react-router-dom"
import { checkCollision } from "./helpers"
import { selectGameStatus, selectSocket, selectUsers } from "../../store/selectors"
import {
  joinRoomSocket,
  leaveRoomSocket,
  startGameSocket,
} from "../../store/actions"

// Custom Hooks
import useGrid from "../../hooks/useGrid"
import usePlayer from "../../hooks/usePlayer"
import useInterval from "../../hooks/useInterval"
import useGameStatus from "../../hooks/useGameStatus"

// Components
import Grid from "./Grid"
import Card from "./Card"
import Lobby from "./Lobby"
import HomeButton from "./HomeButton"
import StartButton from "./StartButtton"
import { StyledTetris, StyledTetrisWrapper } from "../styles/StyledTetris"
import ResultCard from "./ResultCard"
import { setError } from "../../store/reducers/authSlice"
import GameSettings from "../GameSettings"
import useTetriminoPreview from "../../hooks/useTetriminoPreview"
let processing = false

function Tetris() {
  const navigate = useNavigate()
  const dispatch = useDispatch()
  const gameStatus = useSelector(selectGameStatus)
  const socket = useSelector(selectSocket)
  const users = useSelector(selectUsers)
  const isMounted = useRef(true)
  const gridRef = useRef(null)
  let { username, roomName } = useParams()

  const [endGameData, setEndGameData] = useState([])
  const [gravity, setGravity] = useState(1)
  const [tetriminosList, setTetriminosList] = useState("IJLOSTZ")
  const [score, setScore] = useState(0)
  const [lines, setLines] = useState(0)
  const [level, setLevel] = useState(0)
  const [isAdmin, setIsAdmin] = useState(false)
  const [checked, setChecked] = useState(null)
  const [dropTime, setDropTime] = useState(null)
  const [tetriminoPreview, setTetriminoPreview, setNextTetrimino, TetriminoPreview] =
    useTetriminoPreview()
  const [player, updatePlayerPos, resetPlayer, rotateIfPossible, setPlayer] =
    usePlayer(setNextTetrimino)
  const [serverStatus, setServerStatus] = useState(true)

  const [grid, linesCleared] = useGrid(
    player,
    resetPlayer,
    users.length,
    setDropTime,
    setScore,
    setLines,
    setLevel,
    setEndGameData,
    gridRef,
    setPlayer,
    setGravity,
    setTetriminoPreview
  )

  useGameStatus(linesCleared, score, setScore, lines, setLines, level, users.length)

  const closeAlert = () => dispatch(setError(""))

  const connect = () => {
    // check if user is allowed to access this room
    dispatch(
      joinRoomSocket({
        username: username,
        roomName: roomName,
        cb: (response) => {
          if (!response.authorized) {
            dispatch(setError(response.error))
            navigate("/", { replace: true })
          } else {
            setChecked(true)
            closeAlert()
          }
        },
      })
    )
  }

  useEffect(() => {
    dispatch(leaveRoomSocket(connect))
  }, [roomName, username])

  // useEffect hook is called each time one of its deps is modified
  // here the array is empty, it will run once on ComponentDidMount
  // its returned function is called when the components unmounts
  // isMounted ref checks that no re-renders happen while the component is not mounted
  useEffect(() => {
    if (isMounted.current) {
      socket.on("reconnect", () => {
        connect()
        setServerStatus(true)
      })

      socket.on("connect_error", () => {
        setServerStatus(false)
      })
    }
    return () => {
      isMounted.current = false
    }
  }, [])

  // move down each dropTime

  const drop = () => {
    // official speedCurve = https://harddrop.com/wiki/Tetris_Worlds
    if (lines >= (level + 1) * 10) {
      setLevel(level + 1)
      setDropTime(gravity ? ((0.8 - level * 0.007) ** level * 1000) / gravity : 0)
    }
    if (!checkCollision(player, grid, { x: 0, y: 1 })) {
      updatePlayerPos({ x: 0, y: 1, collided: false })
      return false
    }
    updatePlayerPos({ x: 0, y: 0, collided: true })
    return true
  }

  const movePlayer = (direction) => {
    if (!checkCollision(player, grid, { x: direction, y: 0 })) {
      updatePlayerPos({ x: direction, y: 0, collided: false })
    }
  }

  const move = ({ keyCode }) => {
    if (gameStatus === 1 && !player.collided && !processing) {
      // if playing
      processing = true
      if (keyCode === 37) {
        movePlayer(-1)
      } else if (keyCode === 39) {
        movePlayer(1)
      } else if (keyCode === 40) {
        drop()
      } else if (keyCode === 38) {
        rotateIfPossible(grid, 1)
      } else if (keyCode === 32) {
        let y = 0
        while (!checkCollision(player, grid, { x: 0, y: y + 1 })) {
          y++
        }
        updatePlayerPos({ x: 0, y: y })
        drop()
      }
      processing = false
    }
  }

  const EndGameData = () => {
    const user = endGameData.filter((player) => player.name === username)[0]

    return (
      <>
        <Card gameOver={true} looser={user.isLooser} />
        {endGameData.map((user, index) => (
          <ResultCard key={index} user={user} />
        ))}
      </>
    )
  }

  useEffect(() => {
    users.forEach((user) => {
      if (username === user.name) {
        setIsAdmin(user.isAdmin)
      }
    })
  }, [users])

  useInterval(() => {
    while (processing) {
      console.log("...processing...")
    }
    if (serverStatus && !player.collided) {
      processing = true
      if (gravity) drop()
      processing = false
    }
  }, dropTime)

  if (checked === null) {
    return <></>
  }

  return (
    <StyledTetrisWrapper
      role="button"
      tabIndex="0"
      onKeyDown={move}
      data-testid="tetris"
      ref={gridRef}
    >
      <HomeButton />
      {checked && (
        <StyledTetris>
          <Grid grid={grid} />
          <aside>
            <div>
              {gameStatus === 2 && endGameData.length ? (
                <EndGameData />
              ) : (
                gameStatus === 1 && (
                  <>
                    <Card text={`Score: ${score}`} />
                    <Card text={`Lines: ${lines}`} />
                    <Card text={`Level: ${level + 1}`} />
                    <TetriminoPreview />
                  </>
                )
              )}
              <Lobby users={users} />
            </div>
            {isAdmin && gameStatus !== 1 && (
              <>
                <GameSettings
                  defaultOpen={gameStatus === 0}
                  gravity={gravity}
                  setGravity={setGravity}
                  tetriminosList={tetriminosList}
                  setTetriminosList={setTetriminosList}
                  tetriminoPreview={tetriminoPreview}
                  setTetriminoPreview={setTetriminoPreview}
                />
                <StartButton
                  cb={() =>
                    dispatch(
                      startGameSocket({ gravity, tetriminosList, tetriminoPreview })
                    )
                  }
                />
              </>
            )}
          </aside>
        </StyledTetris>
      )}
    </StyledTetrisWrapper>
  )
}

export default Tetris
