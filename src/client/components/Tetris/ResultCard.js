import React from "react"
import StyledResultCard from "../styles/StyledResultCard"
import PropTypes from "prop-types"

const ResultCard = ({ user }) => (
  <StyledResultCard data-testid={"result-card"}>
    {user.name} scored {user.score} points
    <ul>
      <li>Level: {user.level + 1}</li>
      <li>Lines: {user.lines}</li>
    </ul>
  </StyledResultCard>
)

ResultCard.propTypes = {
  user: PropTypes.object,
}

export default ResultCard
