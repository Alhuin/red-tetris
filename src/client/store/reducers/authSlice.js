import { createSlice } from "@reduxjs/toolkit"
import socketio from "socket.io-client"

export const authSlice = createSlice({
  name: "auth",
  initialState: {
    socket: socketio(
      process.env.NODE_ENV === "production" ? "" : "http://localhost:4001"
    ),
    username: "",
    roomName: "",
    error: "",
    users: [],
  },
  reducers: {
    setUsers(state, action) {
      state.users = action.payload
    },
    initUser(state, action) {
      const { username, roomName } = action.payload
      state.username = username
      state.roomName = roomName
    },
    setError(state, action) {
      state.error = action.payload
    },
  },
})

export const { setUsers, initUser, setError } = authSlice.actions
export default authSlice.reducer
