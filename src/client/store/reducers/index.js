import authReducer from "./authSlice"
import gameReducer from "./gameSlice"

export default {
  authReducer: authReducer,
  gameReducer: gameReducer,
}
