import { createSlice } from "@reduxjs/toolkit"

export const gameSlice = createSlice({
  name: "game",
  initialState: {
    gameStatus: 0,
  },
  reducers: {
    setGameStatus(state, action) {
      state.gameStatus = action.payload
    },
  },
})

export const { setGameStatus } = gameSlice.actions
export default gameSlice.reducer
