import { createAction } from "@reduxjs/toolkit"

/**
 *    MiddleWare to SocketIO actions
 */

export const endGameSocket = createAction("socket/endGame")
export const joinRoomSocket = createAction("socket/joinRoom")
export const leaveRoomSocket = createAction("socket/leaveRoom")
export const sendGameDataSocket = createAction("socket/sendGameData")
export const sendShadowSocket = createAction("socket/sendShadow")
export const sendWallSocket = createAction("socket/sendWall")
export const checkRoomSocket = createAction("socket/checkRoomUser")
export const getTetriminoSocket = createAction("socket/getTetrimino")
export const pieceMergedSocket = createAction("socket/pieceMerged")
export const startGameSocket = createAction("socket/startGame")
