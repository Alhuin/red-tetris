import { configureStore } from "@reduxjs/toolkit"
import gameReducer from "./reducers/gameSlice"
import authReducer from "./reducers/authSlice"
import socketMiddleware from "../middlewares/socketMiddleware"

const store = configureStore({
  reducer: {
    game: gameReducer,
    auth: authReducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({ serializableCheck: false }).concat(socketMiddleware),
  devTools: process.env.NODE_ENV !== "production",
})

export default store
