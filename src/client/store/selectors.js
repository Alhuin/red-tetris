import { createSelector } from "@reduxjs/toolkit"

const selectAuth = (state) => state.auth
const selectGame = (state) => state.game

const selectError = createSelector(selectAuth, (state) => state.error)
const selectGameStatus = createSelector(selectGame, (state) => state.gameStatus)
const selectUsers = createSelector(selectAuth, (state) => state.users)
const selectSocket = createSelector(selectAuth, (state) => state.socket)

export { selectError, selectGameStatus, selectSocket, selectUsers }
