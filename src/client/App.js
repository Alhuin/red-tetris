import React from "react"
import "./App.css"
import { Navigate, unstable_HistoryRouter as Router } from "react-router-dom"
import { Route, Routes } from "react-router-dom"
import Tetris from "./components/Tetris"
import ErrorBoundary from "./components/ErrorBoundaries"
import { StyledApp } from "./components/styles/StyledApp"
import { useDispatch, useSelector } from "react-redux"
import { selectSocket } from "./store/selectors"
import { JOIN_ROOM_SUCCESS, USER_LEFT } from "../server/constants/events"
import { setUsers } from "./store/reducers/authSlice"
import { useHashHistory } from "use-hash-history"
import Login from "./components/Login"

function App() {
  const history = useHashHistory({ hashRoot: "" })
  const socket = useSelector(selectSocket)
  const dispatch = useDispatch()

  socket.on(JOIN_ROOM_SUCCESS, (data) => dispatch(setUsers(data.players)))
  socket.on(USER_LEFT, (data) => dispatch(setUsers(data.players)))

  return (
    <StyledApp>
      <Router history={history}>
        <ErrorBoundary>
          <Routes>
            <Route path="/:roomName[:username]" element={<Tetris />} />
            <Route path="/" element={<Login />} />
            <Route path="*" element={<Navigate to={"/"} />} />
          </Routes>
        </ErrorBoundary>
      </Router>
    </StyledApp>
  )
}

export default App
