import React from "react"
import { Provider } from "react-redux"
import "./index.css"
import App from "./App"
import { createRoot } from "react-dom/client"
import store from "./store"

const root = createRoot(document.getElementById("app"))

root.render(
  <Provider store={store}>
    <App />
  </Provider>
)
