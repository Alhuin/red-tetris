/**
 * @jest-environment node
 */
const { createServer } = require("http")
const { Server } = require("socket.io")
const Client = require("socket.io-client")
const {
  END_GAME,
  GET_TETRIMINO,
  JOIN_ROOM,
  JOIN_ROOM_SUCCESS,
  LEAVE_ROOM,
  SEND_GAME_DATA,
  SEND_SHADOW,
  SEND_WALL,
  START_GAME,
  CHECK_ROOM_USER,
  USER_LEFT,
  SET_WALL,
  SET_SHADOW,
  PIECE_MERGED,
} = require("../../constants/events")
const handleSocket = require("../../socketio")

describe("SocketIo testing", () => {
  let io, httpServer
  const sockets = []

  const createNewSocket = (cb) => {
    const port = httpServer.address().port
    const client = new Client(`http://localhost:${port}`)
    client.on("connect", () => cb(client))
    sockets.push(client)
  }

  beforeAll((done) => {
    httpServer = createServer()
    io = new Server(httpServer)
    httpServer.listen(() => {
      io.on("connection", (socket) => {
        handleSocket(socket, io)
      })
      createNewSocket(() => done())
    })
  })

  afterAll(() => {
    httpServer.close()
    io.close()
    sockets.forEach((sock) => sock.close())
  })

  describe("JOIN_ROOM", () => {
    const name = "JoinRoom"
    const roomName = `testRoom${name}`

    test("0: params check", (done) => {
      createNewSocket((socket) => {
        socket.emit(JOIN_ROOM, { username: "", roomName: "" }, (resp) => {
          expect(resp).toEqual({
            authorized: false,
            error: "Missing username or roomName",
          })
          done()
        })
      })
    })

    test("1: _SUCCESS with one user", (done) => {
      createNewSocket((socket) => {
        socket.on(JOIN_ROOM_SUCCESS, (data) => {
          expect(data.players[0].name).toBe(`testUserOne${name}`)
          expect(data.players[0].isAdmin).toBeTruthy()
          done()
        })
        socket.emit(
          JOIN_ROOM,
          { username: `testUserOne${name}`, roomName: roomName },
          (resp) => {
            expect(resp).toEqual({
              authorized: true,
              error: null,
            })
          }
        )
      })
    })

    test("2: _ERROR with same username", (done) => {
      createNewSocket((socket) => {
        socket.emit(
          JOIN_ROOM,
          { username: `testUserOne${name}`, roomName: roomName },
          (resp) => {
            expect(resp).toEqual({
              authorized: false,
              error: "Username already taken",
            })
            done()
          }
        )
      })
    })

    test("3: _SUCCESS with 2 users", (done) => {
      createNewSocket((socket) => {
        socket.on(JOIN_ROOM_SUCCESS, (data) => {
          expect(data.players.length).toBe(2)
          expect(data.players.map((player) => player.name)).toStrictEqual([
            `testUserOne${name}`,
            `testUserThree${name}`,
          ])
          done()
        })
        socket.emit(
          JOIN_ROOM,
          { username: `testUserThree${name}`, roomName: roomName },
          (resp) => {
            expect(resp).toEqual({
              authorized: true,
              error: null,
            })
          }
        )
      })
    })

    test("4: _ERROR with 3 users", (done) => {
      createNewSocket((socket) => {
        socket.emit(
          JOIN_ROOM,
          { username: `testUserFour${name}`, roomName: roomName },
          (resp) => {
            expect(resp).toEqual({
              authorized: false,
              error: "This room is full !",
            })
            done()
          }
        )
      })
    })

    test("5: _ERROR with invalid username", (done) => {
      createNewSocket((socket) => {
        socket.emit(
          JOIN_ROOM,
          { username: `testUser4${name}`, roomName: roomName },
          (resp) => {
            expect(resp).toEqual({
              authorized: false,
              error:
                "Inputs must be at least 3 length, can contain only cap/min letters.",
            })
            done()
          }
        )
      })
    })

    test("6: _ERROR with invalid roomName", (done) => {
      createNewSocket((socket) => {
        socket.emit(
          JOIN_ROOM,
          { username: `testUserFour${name}`, roomName: `1${roomName}` },
          (resp) => {
            expect(resp).toEqual({
              authorized: false,
              error:
                "Inputs must be at least 3 length, can contain only cap/min letters.",
            })
            done()
          }
        )
      })
    })

    test("7: _ERROR with game already started", (done) => {
      createNewSocket((socket) => {
        sockets[4].on("start", () => {
          socket.emit(
            JOIN_ROOM,
            { username: `testUserFive${name}`, roomName: roomName },
            (resp) => {
              expect(resp).toEqual({
                authorized: false,
                error: "An epic battle is already happening here !",
              })
              done()
            }
          )
        })
        sockets[4].emit(START_GAME, {
          gravity: 1,
          tetriminosList: "IJLOSTZ",
          tetriminoPreview: false,
        })
      })
    })
  })

  describe("CHECK_ROOM_USER", () => {
    const name = "checkRoomUser"

    test("authorized", () => {
      const roomName = `testRoomZero${name}`
      const username = `testUserZero${name}`

      createNewSocket((socket) => {
        socket.emit(CHECK_ROOM_USER, { username, roomName }, (data) => {
          expect(data).toStrictEqual({ authorized: true, error: null })
        })
      })
    })
  })

  describe("END_GAME", () => {
    const name = "EndGame"

    test("0: Stop the game, raise end event", (done) => {
      const username = `testUser${name}`
      const roomName = `testRoom${name}`

      createNewSocket((socket) => {
        socket.on("end", (data) => {
          // THEN
          // Returned data is good
          expect(data).toStrictEqual([
            {
              name: username,
              score: 4242,
              level: 42,
              lines: 42,
              isLooser: true,
            },
          ])
          // New user can join (game stopped)
          createNewSocket((socket2) => {
            socket2.emit(
              JOIN_ROOM,
              { username: `testUserTwo${name}`, roomName },
              (res) => {
                expect(res).toEqual({ authorized: true, error: null })
                done()
              }
            )
          })
        })
        socket.on("start", () => {
          socket.emit(SEND_GAME_DATA, roomName, {
            score: 4242,
            lines: 42,
            level: 42,
          })
          // WHEN
          socket.emit(END_GAME)
        })
        socket.emit(JOIN_ROOM, { username, roomName }, (response) => {
          // WITH
          expect(response).toEqual({ authorized: true, error: null })
          socket.emit(START_GAME, {
            gravity: 1,
            tetriminosList: "IJLOSTZ",
            tetriminoPreview: false,
          })
        })
      })
    })
  })

  describe("START_GAME", () => {
    test("0: return null if game is not found", (done) => {
      createNewSocket((socket) => {
        const mock = jest.fn()
        socket.on("start", mock)
        socket.emit(START_GAME, {
          gravity: 1,
          tetriminosList: "IJ",
          tetriminoPreview: false,
        })
        expect(mock).not.toHaveBeenCalled()
        done()
      })
    })
  })

  describe("DISCONNECT", () => {
    const name = "Disconnect"

    test("0: Room is leaved, admin changed", (done) => {
      const roomName = `testRoomZero${name}`
      const userStaying = `testUserStayZero${name}`
      const userLeaving = `testUserLeaveZero${name}`

      createNewSocket((socket) => {
        socket.emit(JOIN_ROOM, { username: userLeaving, roomName }, (response) => {
          expect(response).toEqual({ authorized: true, error: null })
          createNewSocket((socket2) => {
            socket2.on(USER_LEFT, (data) => {
              expect(data.players.length).toBe(1)
              expect(data.players[0].name).toBe(userStaying)
              expect(data.players[0].isAdmin).toBeTruthy()
              done()
            })
            socket2.emit(JOIN_ROOM, { username: userStaying, roomName }, () =>
              socket.close()
            )
          })
        })
      })
    })

    test("1: Finish game if game launched", (done) => {
      const roomName = `testRoomOne${name}`
      const userStaying = `testUserStayOne${name}`
      const userLeaving = `testUserLeaveOne${name}`

      createNewSocket((socket) => {
        socket.emit(JOIN_ROOM, { username: userLeaving, roomName }, (response) => {
          expect(response).toEqual({ authorized: true, error: null })
          socket.on("start", socket.close)
          createNewSocket((socket2) => {
            socket2.on("end", (data) => {
              expect(data).toStrictEqual([
                {
                  name: userLeaving,
                  score: 0,
                  level: 0,
                  lines: 0,
                  isLooser: true,
                },
                {
                  name: userStaying,
                  score: 0,
                  level: 0,
                  lines: 0,
                  isLooser: false,
                },
              ])
              done()
            })

            socket2.emit(
              JOIN_ROOM,
              { username: userStaying, roomName },
              (response) => {
                expect(response).toEqual({ authorized: true, error: null })
                socket2.emit(START_GAME, {
                  gravity: 1,
                  tetriminosList: "IJLOSTZ",
                  tetriminoPreview: false,
                })
              }
            )
          })
        })
      })
    })
  })

  describe("LEAVE_ROOM", () => {
    const name = "LeaveRoom"

    test("0: Room is leaved, admin changed", (done) => {
      const roomName = `testRoomZero${name}`
      const userStaying = `testUserStayZero${name}`
      const userLeaving = `testUserLeaveZero${name}`

      createNewSocket((socket) => {
        socket.emit(JOIN_ROOM, { username: userLeaving, roomName }, (response) => {
          expect(response).toEqual({ authorized: true, error: null })
          createNewSocket((socket2) => {
            socket2.on(USER_LEFT, (data) => {
              expect(data.players.length).toBe(1)
              expect(data.players[0].name).toBe(userStaying)
              expect(data.players[0].isAdmin).toBeTruthy()
              done()
            })
            socket2.emit(
              JOIN_ROOM,
              { username: userStaying, roomName },
              (response) => {
                expect(response).toEqual({ authorized: true, error: null })
                socket.emit(LEAVE_ROOM)
              }
            )
          })
        })
      })
    })

    test("1: Finish game if game launched", (done) => {
      const roomName = `testRoomOne${name}`
      const userStaying = `testUserStayOne${name}`
      const userLeaving = `testUserLeaveOne${name}`

      createNewSocket((socket) => {
        socket.emit(JOIN_ROOM, { username: userLeaving, roomName }, (response) => {
          expect(response).toEqual({ authorized: true, error: null })
          createNewSocket((socket2) => {
            socket2.on("end", (data) => {
              expect(data).toStrictEqual([
                {
                  name: userLeaving,
                  score: 0,
                  level: 0,
                  lines: 0,
                  isLooser: true,
                },
                {
                  name: userStaying,
                  score: 0,
                  level: 0,
                  lines: 0,
                  isLooser: false,
                },
              ])
              done()
            })
            socket.on("start", () => socket.emit(LEAVE_ROOM))
            socket2.emit(
              JOIN_ROOM,
              { username: userStaying, roomName },
              (response) => {
                expect(response).toEqual({ authorized: true, error: null })
                socket.emit(START_GAME, {
                  gravity: 1,
                  tetriminosList: "IJLOSTZ",
                  tetriminoPreview: false,
                })
              }
            )
          })
        })
      })
    })

    test("2: Leaved user.isAdmin is reset", (done) => {
      const roomName = `testRoomTwo${name}`
      const userStaying = `testUserStayTwo${name}`
      const userLeaving = `testUserLeaveAndRejoinTwo${name}`

      createNewSocket((socket) => {
        socket.emit(JOIN_ROOM, { username: userLeaving, roomName }, (response) => {
          expect(response).toEqual({ authorized: true, error: null })
          createNewSocket((socket2) => {
            socket2.on(USER_LEFT, (data) => {
              expect(data.players.length).toBe(1)
              expect(data.players[0].name).toBe(userStaying)
              expect(data.players[0].isAdmin).toBeTruthy()
              socket.on(JOIN_ROOM_SUCCESS, (data) => {
                expect(data.players.length).toBe(2)
                const userAdmin = data.players.find((player) => player.isAdmin)
                expect(userAdmin.name).toBe(userStaying)
                const userNotAdmin = data.players.find((player) => !player.isAdmin)
                expect(userNotAdmin.name).toBe(userLeaving)
                done()
              })
              socket.emit(JOIN_ROOM, { username: userLeaving, roomName }, () => {})
            })
            socket2.emit(
              JOIN_ROOM,
              { username: userStaying, roomName },
              (response) => {
                expect(response).toEqual({ authorized: true, error: null })
                socket.emit(LEAVE_ROOM)
              }
            )
          })
        })
      })
    })
  })

  describe("GET_TETRIMINO", () => {
    const name = "GetTetrimino"

    test("0: Return a random tetrimino if the room is launched", (done) => {
      const roomName = `testRoomZero${name}`
      const username = `testUserZero${name}`

      createNewSocket((socket) => {
        socket.on("start", () => {
          socket.emit(GET_TETRIMINO, roomName, (response) => {
            expect("IJLOSTZ").toContain(response.tetrimino)
            socket.emit(PIECE_MERGED, roomName)
            socket.emit(GET_TETRIMINO, roomName, (response2) => {
              expect(response2).not.toBe(response)
              done()
            })
          })
        })
        socket.emit(JOIN_ROOM, { username, roomName }, (response) => {
          expect(response).toEqual({ authorized: true, error: null })
          socket.emit(START_GAME, {
            gravity: 1,
            tetriminosList: "IJLOSTZ",
            tetriminoPreview: false,
          })
        })
      })
    })

    test("1: Return null when room not launched", (done) => {
      createNewSocket((socket) => {
        const mock = jest.fn()
        socket.emit(GET_TETRIMINO, "notAValidRoomName", mock)
        expect(mock).not.toHaveBeenCalled()
        done()
      })
    })
  })

  describe("SEND_WALL", () => {
    const name = "SendWall"

    test("0: Raise Set Wall on event", (done) => {
      const roomName = `testRoomZero${name}`
      const sender = `testUserZero${name}`
      const receiver = `testUserOne${name}`

      createNewSocket((socket) => {
        createNewSocket((socket2) => {
          socket2.on(SET_WALL, (walls) => {
            expect(walls).toBe(2)
            done()
          })
          socket.on("start", () => socket.emit(SEND_WALL, roomName, 2))
          socket.on(JOIN_ROOM_SUCCESS, (response) => {
            if (response.players.length === 2) {
              socket.emit(START_GAME, {
                gravity: 1,
                tetriminosList: "IJO",
                tetriminoPreview: false,
              })
            }
          })
          socket.emit(JOIN_ROOM, { username: sender, roomName }, (response) => {
            expect(response).toEqual({ authorized: true, error: null })
          })
          socket2.emit(JOIN_ROOM, { username: receiver, roomName }, (response) => {
            expect(response).toEqual({ authorized: true, error: null })
          })
        })
      })
    })
  })

  describe("SEND_SHADOW", () => {
    const name = "SendShadow"

    test("0: Raise Set Shadow on event", (done) => {
      const roomName = `testRoomZero${name}`
      const sender = `testUserZero${name}`
      const receiver = `testUserOne${name}`

      createNewSocket((socket) => {
        createNewSocket((socket2) => {
          socket2.on(SET_SHADOW, (shadow) => {
            expect(shadow).toStrictEqual([21, 42])
            done()
          })
          socket.on("start", () => socket.emit(SEND_SHADOW, roomName, [21, 42]))
          socket.on(JOIN_ROOM_SUCCESS, (response) => {
            if (response.players.length === 2) {
              socket.emit(START_GAME, {
                gravity: 1,
                tetriminosList: "IJO",
                tetriminoPreview: false,
              })
            }
          })
          socket.emit(JOIN_ROOM, { username: sender, roomName }, (response) => {
            expect(response).toEqual({ authorized: true, error: null })
          })
          socket2.emit(JOIN_ROOM, { username: receiver, roomName }, (response) => {
            expect(response).toEqual({ authorized: true, error: null })
          })
        })
      })
    })
  })
})
