const { v4 } = require("uuid")

class Game {
  constructor(roomName) {
    this.id = v4().toString()
    this.name = roomName
    this.players = []
    this.launched = false
    this.tetriminosList = ""
    this.tetriminosChoices = ""
  }
}

module.exports = Game
