const { v4 } = require("uuid")

class Player {
  constructor(socket) {
    this.id = v4().toString()
    this.name = ""
    this.socket = socket
    this.game = ""
    this.isAdmin = false
    this.score = 0
    this.lines = 0
    this.level = 0
    this.tetrimino_index = 0
  }
}

module.exports = Player
