class Piece {
  constructor(tetriminosChoices) {
    this.ref = this.random(tetriminosChoices)
  }

  random = (tetriminosChoices) =>
    tetriminosChoices[Math.floor(Math.random() * tetriminosChoices.length)]
}

module.exports = Piece
