const Player = require("./Player")
const Game = require("./Game")
const Piece = require("./Piece")

module.exports = {
  Player,
  Game,
  Piece,
}
