const express = require("express")
const http = require("http")
const cors = require("cors")
const { Server } = require("socket.io")
const handleSocket = require("./socketio")

const port = process.env.PORT || 4001
const app = express()

app.use(cors())
app.options("*", cors())
app.use(express.static("public"))

const server = http.createServer(app)
const options =
  process.env.NODE_ENV === "production"
    ? {}
    : {
        cors: {
          origin: "http://localhost:8080",
          methods: ["GET", "POST"],
        },
      }

const io = new Server(server, options)

io.on("connection", (socket) => handleSocket(socket, io))

server.listen(port, () => console.log(`Listening on port ${port}`))

const servConfig = { server, io }
module.exports = servConfig
