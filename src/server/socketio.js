const debug = require("debug")
const { Player, Game, Piece } = require("./models")
const { events } = require("./constants")

const logger = debug("tetris:info")
const loginfo = ({ eventName, username, roomName, message, sending }) => {
  let finalString = sending ? "<= " : "=> "
  finalString += eventName ? `${eventName.padEnd(18, " ")}: ` : ""
  finalString += username ? `username: ${username.padEnd(10, " ")}| ` : ""
  finalString += roomName ? `roomName: ${roomName.padEnd(10, " ")}| ` : ""
  finalString += message ? `${message}` : ""
  logger(finalString)
}

const games = {}

const createPlayersList = (game) =>
  game.players.map((player) => ({
    name: player.name,
    id: player.id,
    isAdmin: player.isAdmin,
  }))

const createEndGameData = (game, looser) =>
  game.players.map((player) => ({
    name: player.name,
    score: player.score,
    lines: player.lines,
    level: player.level,
    isLooser: player.id === looser.id,
  }))

const joinRoom = (io, socket, game, player, cb) => {
  // Join socket.io room
  socket.join(game.name)

  // Save roomId in socket
  player.game = game.name

  // Add socket in db room
  game.players.push(player)

  if (game.players.length === 1) {
    player.isAdmin = true
  }
  cb({ authorized: true, error: null })
  const players = createPlayersList(game)
  loginfo({
    eventName: events.JOIN_ROOM_SUCCESS,
    roomName: game.name,
    message: JSON.stringify(players),
    sending: true,
  })
  io.in(game.name).emit(events.JOIN_ROOM_SUCCESS, { players })
  return true
}

const leaveRoom = (socket, player, io) => {
  const game = games[player.game]

  socket.leave(game.id)
  game.players = game.players.filter((user) => user.id !== player.id)
  player.isAdmin = false
  if (game.players.length === 0) {
    loginfo({
      roomName: game.name,
      message: `Game ${game.name} is empty, deleting...`,
      sending: true,
    })
    delete games[game.id]
    return
  }
  game.players[0].isAdmin = true
  const players = createPlayersList(game)
  loginfo({
    eventName: events.USER_LEFT,
    roomName: game.name,
    sending: true,
  })
  io.in(game.name).emit(events.USER_LEFT, { players })
}

const finishGame = (player, io) => {
  const game = games[player.game]
  if (game.launched) {
    const endGameData = createEndGameData(game, player)
    loginfo({
      eventName: "end",
      roomName: game.name,
      message: JSON.stringify(endGameData),
      sending: true,
    })
    io.in(game.name).emit("end", endGameData)
  }
  game.players.forEach((player) => {
    player.score = 0
    player.lines = 0
    player.level = 0
    player.tetrimino_index = 0
  })
  game.tetriminosChoices = ""
  game.tetriminosList = ""
  game.launched = false
}

const leaveAndFinish = (socket, io, player) => {
  if (!player.game) return
  finishGame(player, io)
  leaveRoom(socket, player, io)
}

function handleSocket(socket, io) {
  loginfo({ message: `New client connected with id ${socket.id}` })

  const player = new Player(socket)

  const checkRoom = (username, roomName) => {
    const game = games[roomName]

    if (!username || !roomName) {
      return { authorized: false, error: "Missing username or roomName" }
    }
    if (!username.match("^[a-zA-Z]{3,}$") || !roomName.match("^[a-zA-Z]{3,}$")) {
      return {
        authorized: false,
        error: "Inputs must be at least 3 length, can contain only cap/min letters.",
      }
    }
    if (!game) {
      return { authorized: true, error: null }
    }
    if (game.players.find((player) => player.name === username)) {
      return { authorized: false, error: "Username already taken" }
    }
    if (game.launched) {
      return {
        authorized: false,
        error: "An epic battle is already happening here !",
      }
    }
    if (game.players.length >= 2) {
      return {
        authorized: false,
        error: "This room is full !",
      }
    }
    return { authorized: true, error: null }
  }

  socket.on(events.CHECK_ROOM_USER, ({ username, roomName }, callback) => {
    const message = checkRoom(username, roomName)
    loginfo({
      eventName: events.CHECK_ROOM_USER,
      username,
      roomName,
      message: JSON.stringify(message),
    })
    callback(message)
  })

  socket.on(events.JOIN_ROOM, ({ username, roomName }, callback) => {
    let game = games[roomName]
    const check_response = checkRoom(username, roomName)
    loginfo({
      eventName: events.JOIN_ROOM,
      username,
      roomName,
      message: JSON.stringify(check_response),
    })

    if (!check_response.authorized) {
      callback(check_response)
      return
    }
    player.name = username

    if (game === undefined) {
      // create room
      game = new Game(roomName)
      player.game = game.name
      games[game.name] = game
    }
    joinRoom(io, socket, game, player, callback)
  })

  socket.on(events.PIECE_MERGED, (roomName) => {
    const game = games[roomName]

    if (game) {
      player.tetrimino_index += 1
    }
  })

  socket.on(events.GET_TETRIMINO, (roomName, cb) => {
    const game = games[roomName]

    if (!game) {
      loginfo({
        eventName: events.GET_TETRIMINO,
        username: player.name,
        roomName,
        message: `<= Room ${roomName} not found`,
      })
      return
    }

    if (player.tetrimino_index + 1 === game.tetriminosList.length) {
      game.tetriminosList += new Piece(game.tetriminosChoices).ref
    }

    const newTetrimino = game.tetriminosList[player.tetrimino_index]
    const nextTetrimino = game.tetriminosList[player.tetrimino_index + 1]

    loginfo({
      eventName: events.GET_TETRIMINO,
      username: player.name,
      roomName,
      message: `<= tetrimino: ${newTetrimino}`,
    })
    cb({ tetrimino: newTetrimino, nextTetrimino })
  })

  socket.on(events.SEND_GAME_DATA, (roomName, data) => {
    const game = games[roomName]
    loginfo({
      eventName: events.SEND_GAME_DATA,
      username: player.name,
      roomName,
      message: `Room ${roomName}${game ? "" : " not"} found`,
    })

    if (game) {
      const sender = game.players.find((user) => user.socket.id === socket.id)
      sender.score = data.score
      sender.lines = data.lines
      sender.level = data.level
    }
  })

  socket.on(events.SEND_SHADOW, (roomName, shadow) => {
    const game = games[roomName]
    loginfo({
      eventName: events.SEND_SHADOW,
      username: player.name,
      roomName,
      message: `Room ${roomName}${game ? "" : " not"} found`,
    })

    if (game && shadow && game.players.length > 1) {
      const receiver = game.players.find((user) => user.socket.id !== socket.id)
      loginfo({
        eventName: events.SET_SHADOW,
        username: receiver.name,
        roomName,
        message: `Shadow: ${shadow}`,
        sending: true,
      })
      socket.to(receiver.socket.id).emit(events.SET_SHADOW, shadow)
    }
  })

  socket.on(events.SEND_WALL, (roomName, wall) => {
    const game = games[roomName]
    loginfo({
      eventName: events.SEND_WALL,
      username: player.name,
      roomName,
      message: `Room ${roomName}${game ? "" : " not"} found`,
    })

    if (game && wall && game.players.length > 1) {
      const receiver = game.players.find((user) => user.socket.id !== socket.id)
      loginfo({
        eventName: events.SEND_WALL,
        username: receiver.name,
        roomName,
        message: `Wall: ${wall}`,
        sending: true,
      })
      socket.to(receiver.socket.id).emit(events.SET_WALL, wall)
    }
  })

  socket.on(events.END_GAME, () => {
    loginfo({
      eventName: events.END_GAME,
      username: player.name,
      roomName: player.game.name,
    })
    finishGame(player, io)
  })

  socket.on(events.START_GAME, ({ gravity, tetriminosList, tetriminoPreview }) => {
    const game = games[player.game]
    loginfo({
      eventName: events.START_GAME,
      username: player.name,
      roomName: game ? game.name : "not found",
      message: `received gameSettings gravity: ${gravity}, tetriminosChoices: ${tetriminosList}`,
    })
    if (!game) {
      return
    }

    game.tetriminosChoices = tetriminosList
    game.tetriminosList += new Piece(tetriminosList).ref
    loginfo({
      eventName: "start",
      roomName: game.name,
      sending: true,
    })
    io.in(game.name).emit("start", { gravity, tetriminoPreview })
    game.launched = true
  })

  socket.on(events.DISCONNECT, () => {
    loginfo({
      eventName: events.DISCONNECT,
      username: player.name,
      roomName: player.game.name,
    })
    leaveAndFinish(socket, io, player)
  })

  socket.on(events.LEAVE_ROOM, (callback) => {
    loginfo({
      eventName: events.LEAVE_ROOM,
      username: player.name,
      roomName: player.game.name,
    })
    leaveAndFinish(socket, io, player)
    if (callback) callback()
  })
}

module.exports = handleSocket
