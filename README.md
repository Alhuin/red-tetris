# Getting Started

- `git clone git@gitlab.com:Alhuin/red-tetris.git`
- `npm install`

## Development Build

- `npm run client-dev`
- `npm run srv dev`
- client: http://localhost:8080
- server: http://localhost:4001

## Production Build
- `npm run build`
- `npm start`
- app: http://localhost:4001

## Heroku
- staging: https://it42-redtetris-staging.herokuapp.com/
- production: https://it42-redtetris-production.herokuapp.com/