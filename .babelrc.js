module.exports = (api) => {
  api.cache(true)

  const presets = [
    "@babel/preset-env",
    [
      "@babel/preset-react",
      {
        runtime: "automatic",
        development: process.env.NODE_ENV === "development",
      },
    ],
  ]
  const plugins = ["@babel/plugin-proposal-class-properties"]

  return {
    presets,
    plugins,
  }
}
