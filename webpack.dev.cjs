const { merge } = require('webpack-merge');
const common = require('./webpack.common.cjs');
const serverConfig = require('./webpack.server.cjs');

clientConfig = merge(common, {
  devtool: 'inline-source-map',
  devServer: {
    static: {
      directory: 'public'
    },
    hot: true,
  },
});

module.exports = [clientConfig, serverConfig]
