const { merge } = require('webpack-merge');
const TerserPlugin = require('terser-webpack-plugin');
const common = require('./webpack.common.cjs');
const serverConfig = require('./webpack.server.cjs');

clientConfig = merge(common, {
  devtool: 'source-map',
  optimization: {
    minimizer: [
      new TerserPlugin({
        terserOptions: {
          compress: {
            drop_console: true,
          },
          // parallel: true,
          sourceMap: true,
        },
      }),
    ],
  },
  performance: {
    hints: false,
  },
});

module.exports = [clientConfig, serverConfig]
